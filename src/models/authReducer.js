import keyMirror from '../utils/keyMirror';

import { isAuth } from '../utils/auth';

const actionTypes = keyMirror(['loginUser', 'logoutUser']);


const loginUser = user => dispatch =>
    dispatch ({
        type: actionTypes.loginUser,
        payload: user
    });

const logoutUser = ()  => dispatch =>
    dispatch({
        type: actionTypes.logoutUser
    });


const INITIAL_STATE = {
    ...isAuth()

};

export default (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case actionTypes.loginUser:
            return { ...state, user: payload, isAuth: true };
        case actionTypes.logoutUser:
            return { ...state, user: {}, isAuth: false };
        default:
            return state;
    }
};


export { loginUser, logoutUser };
