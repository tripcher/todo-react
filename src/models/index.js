import { combineReducers } from 'redux';

import auth from './authReducer.js';

const reducers = combineReducers({
    auth
});

export default reducers;
