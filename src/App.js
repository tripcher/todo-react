import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';


import Router from './routes/Router.js';
import store from './utils/createStore';
import './App.css';

class App extends React.Component {
  render() {
    return (
        <div className="App">
          <Provider store={store}>
            <BrowserRouter>
              <Router />
            </BrowserRouter>
          </Provider>
        </div>
    );
  }
}

export default App;
