import React from 'react';
import { Route, Switch } from 'react-router-dom';

import config from './config';

const Router = () => (
  <Switch>
    {config.map(({ path, component, name }) => (
      <Route key={name} path={path} component={component} />
    ))}
  </Switch>
);



export default Router;
