import { asyncComponent } from 'react-async-component';

const routerConfig = [
  {
    name: 'Login page',
    component: asyncComponent({
      resolve: () => import('../layouts/login/Login.js')
    }),
    path: '/login'
  }
];

export default routerConfig;
