import { applyMiddleware, compose, createStore as createReduxStore } from 'redux';
import thunk from 'redux-thunk';

import reducers from '../models';

const createStore = () => {
  let composeEnhancers = compose;
  const middleware = [thunk];
  const enhancers = [];

  if (process.env.NODE_ENV !== 'production') {
    /* eslint-disable */
    if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    }
    /* eslint-enable */
  }

  return createReduxStore(reducers, composeEnhancers(applyMiddleware(...middleware), ...enhancers));
};


export default createStore();
