const auth = ({email, password}) => {
    let user = {
        'email': email,
        'username': 'test'
    };
    login(user);
    return user
};

const login = (user) => {
    let serialObj = JSON.stringify(user);
    localStorage.setItem('user', serialObj);
};

const logout = () => {
    localStorage.removeItem('user');
};

const isAuth = () => {
    let user = localStorage.getItem('user');
    return user ? {user: user, isAuth:true} : {user: user, isAuth:false}
};

export {login, logout, isAuth, auth}