import React from 'react';
import PropTypes from 'prop-types';


const Task = ({ title, text, user, organization, date_created }) => (
    <div className="task">
        <div className="task__title">
            {title}
        </div>
        <div className="task__wrap">
            <div className="task__user">
                {user}
            </div>
            <div className="task__organization">
                {organization}
            </div>
        </div>
        <div className="task__date">
            {date_created}
        </div>
    </div>
);

export default Task;