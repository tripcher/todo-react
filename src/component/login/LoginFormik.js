import React from 'react';
import { Formik, FormikProps} from 'formik';
import * as yup from 'yup';
import {withRouter} from "react-router-dom";
import { connect } from 'react-redux';
import { compose } from 'recompose';

import {loginUser, logoutUser} from "../../models/authReducer";
import LoginForm from "./LoginForm";
import { auth, logout } from "../../utils/auth"


class LoginFormik extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: []
        };

        this.schema = yup.object().shape({
            email: yup
                .string()
                .email()
                .required(),
            password: yup
                .string()
                .required(),
        })
    }

    componentDidMount(): void {
        logout();
        this.props.logoutUser();
    }


    handleSubmit = (values, actions) => {
        let user = auth(values);
        if (user) {
            this.props.history.push('/');
            this.props.loginUser(user)
        }else {
            this.setState((state, props) => ({errors: ['Ошибка']}))
        }
        actions.setSubmitting(false);
    };

    renderForm = (props: FormikProps<Values>) => (
        <LoginForm
            props={props}
        />
    );

    render = () => (
        <React.Fragment>
            {
                this.state.errors.length > 0 &&
                <div className="errors">
                    {
                        this.state.errors.map( item => (
                            <div className="errors__element">
                                {item}
                            </div>
                        ))
                    }
                </div>
            }
            <Formik
                initialValues={{
                    email: '',
                    password: ''
                }}
                onSubmit={this.handleSubmit}
                validationSchema={this.schema}
                render={this.renderForm}
            />
        </React.Fragment>
    )
}

const dispatchToProps = {
    loginUser,
    logoutUser
};

const enhance = compose(
    connect(
        null,
        dispatchToProps
    )
);

export default withRouter(enhance(LoginFormik));