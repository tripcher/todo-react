import React from 'react';

import { FormikProps, Form, Field, ErrorMessage} from 'formik';


const LoginForm = (props: FormikProps<Values>) => (
        <Form>
            <Field
                type="text"
                name="email"
                placeholder="email"
                className='input'
            />
            <ErrorMessage name="email" />
            <Field
                type="password"
                name="password"
                placeholder="password"
                className='input'
            />
            <ErrorMessage name="password" />
            <button
                type="submit"
                disabled={props.isSubmitting}
            >
                Submit
            </button>
        </Form>
    );


export default LoginForm;